import os
import shutil

ignore = set(['BUILD_LINKS.py', 'PRUNE_LINKS.py'])
always_merge = set(['ssh'])

default_overwrite=None
default_dir_overwrite=None

def convert_path(dirname, in_path):
    source = dirname +"/" + in_path

    if dirname[0] == ".":
        dirname = dirname[2:]

    if dirname == "":
        target = "~/." + in_path
    else:
        target = "~/." + dirname + "/" + in_path

    return os.path.abspath(source), os.path.expanduser(target)

def is_linked(source, target):
    if os.path.islink(target):
        link_target = os.path.abspath(os.readlink(target))

        if link_target == source:
            return True, True
        else:
            print "\t", source, "currently linked to -> ", link_target
            return True, False
    return False, None

def process_file(dirname, filename):
    global default_overwrite
    overwrite = default_overwrite

    source, target = convert_path(dirname, filename)

    link, same = is_linked(source, target)
    if link and same:
        print "\t", target, "Already linked"
        return True

    if os.path.isdir(target):
        raise Exception("Called process_file on folder")

    if os.path.isfile(target):
        if overwrite is None:
            r = raw_input("\t" + target + " exists would you like to overwrite? [yes/No/all/none]: ")
            if r == "yes":
                overwrite = True
            elif r == "all":
                default_overwrite = True
                overwrite = True
            elif r == "none":
                default_overwrite = False
                overwrite = False
            else:
                overwrite = False

        if overwrite:
            os.remove(target)
        else:
            print "\t", target, "exists as file"
            return False

    print "\t", "Linking", target
    os.symlink(source, target)
    return True

def process_dir(dirname, subdirname):
    global default_dir_overwrite
    overwrite = default_dir_overwrite
    source, target = convert_path(dirname, subdirname)

    link, same = is_linked(source, target)
    if link and same:
        print "\t", target, "Already linked"
        return True

    if os.path.exists(target):
        if not os.path.isdir(target):
            raise Exception("Called process_dir on file")

        if subdirname in always_merge:
            overwrite = "merge"
        if overwrite is None:
            r = raw_input("\t" + target + " exists would you like to overwrite directory? [merge/remove/merge_all/remove_all/Ignore/ignore_all]: ")
            if r == "merge_all":
                default_dir_overwrite = "merge"
                overwrite = "merge"

            elif r == "remove_all":
                default_dir_overwrite = "remove"
                overwrite = "remove"

            elif r == "ignore_all":
                default_dir_overwrite = "ignore"
                overwrite = "ignore"

            elif "merge".startswith(r):
                overwrite = "merge"
            elif "remove".startswith(r):
                overwrite = "remove"
            elif "ignore".startswith(r):
                overwrite = "ignore"

        if overwrite == "remove":
            if link:
                os.remove(target)
            else:
                shutil.rmtree(target)
        elif overwrite == "merge":
            return False
        elif overwrite == "ignore":
            return True
        else:
            return True

    print "\t", "Linking", target
    os.symlink(source, target)
    return True

def main():

    for dirname, dirnames, filenames in os.walk('.'):
        print "Processing:", os.path.abspath(dirname) + "/"

        for subdirname in dirnames[:]:
            if subdirname in ignore or subdirname[0] == '.':
                print "skipping", subdirname + "/"
                dirnames.remove(subdirname)
                continue
            
            ## if process returns true then its either linked it or asked for it to be skipped
            if process_dir(dirname, subdirname):
                dirnames.remove(subdirname)

        for filename in filenames:
            if filename not in ignore and filename[0] != ".": 
                process_file(dirname, filename)
            else:
                print "skipping", dirname + "/" + filename

if __name__ == "__main__":
    main()
