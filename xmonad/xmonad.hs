import XMonad
import XMonad.Actions.OnScreen
import XMonad.Actions.SpawnOn
import XMonad.Layout.Named
import XMonad.Layout.IM
import XMonad.Layout.Grid
import XMonad.Layout.PerWorkspace
import XMonad.Hooks.DynamicLog
import XMonad.Layout.NoBorders
import XMonad.Hooks.ManageDocks
import XMonad.Util.WorkspaceCompare
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run(spawnPipe)
import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import XMonad.Util.EZConfig
import System.IO
import System.Exit
import XMonad.Actions.Warp

import Data.List -- use for find
import Data.Ord -- use for comparing
import Data.Maybe -- use for fromjust
import Data.Ratio -- used for 1%2

-- My Modules
import qualified ScreenManager as SM

--Basic Settings
myBorderWidth = 3

-- Workspace names and Physical Screen order
myWorkspaces = ["1:main", "2","3","4","5","6:media","7:email","8:chat","9:music"]

screenSettings = SM.buildScreenSettings
    [ ("CC", (0, xK_d, "1:main"))
    , ("BL", (1, xK_s, "2"))
    , ("BR", (2, xK_f, "3"))
    , ("TL", (3, xK_e, "8:chat"))
    , ("TR", (4, xK_r, "9:music"))
    , ("LL", (5, xK_a, "4"))
    ]
    ["LL", "BL", "CC", "BR", "TL", "TR"]

xmobarScreen = "CC"

-- Program Definitons

myPrograms = M.fromList
    [ ("terminal", "urxvt")
    , ("browser", "chromium")
    , ("chat", "pidgin")
    , ("music", "clementine")
    , ("media", "vlc")
    , ("lock", "i3lock")
    , ("media-pauseplay", "spotify_remote.sh playpause")
    , ("media-previous", "spotify_remote.sh previous")
    , ("media-next", "spotify_remote.sh next")
    ]

getProgram :: String -> String
getProgram programName = fromJust $ M.lookup programName myPrograms

-- Program Automatic Workspace Mappings
myManageHooks = composeAll
    [ 
      className                       =? "gmail" --> doShift "7:email"
    , className                       =? "Clementine" --> doShift "9:music"
    , className                       =? "Vlc" --> doShift "6:media"
    , stringProperty "WM_WINDOW_ROLE" =? "buddy_list" --> doShift "8:chat"
    , stringProperty "WM_WINDOW_ROLE" =? "conversation" --> doShift "8:chat"
    ]

-- Startup Applications

workspaceLookup = SM.lookupScreenWorkspace screenSettings
screenIdLookup = SM.lookupScreenId screenSettings

myStartupHook :: X ()
myStartupHook = do
    spawnOn "1:main" $ getProgram "terminal";
    spawnOn "3" $ getProgram "browser";
    spawnOn "8:chat" $ getProgram "chat"
    spawnOn "9:music" $ getProgram "music";

	spawn "nitrogen --restore";
    windows (greedyViewOnScreen (screenIdLookup "LL") (workspaceLookup "LL") );
    windows (greedyViewOnScreen (screenIdLookup "BL") (workspaceLookup "BL") );
    windows (greedyViewOnScreen (screenIdLookup "BR") (workspaceLookup "BR") );
    windows (greedyViewOnScreen (screenIdLookup "TL") (workspaceLookup "TL") );
    windows (greedyViewOnScreen (screenIdLookup "TR") (workspaceLookup "TR") );
    windows (greedyViewOnScreen (screenIdLookup "CC") (workspaceLookup "CC") );
    screenWorkspace 0 >>= flip whenJust (windows . W.view)

-- Scratchpads
scratchpads = [
    NS "spotify" "spotify" (className =? "Spotify")
        (customFloating $ W.RationalRect (0.1) (0.1) (0.8) (0.8))

    ]

-- layouts
--
imLayout = avoidStruts $ withIM (1%5) (And (ClassName "Pidgin") (Role "buddy_list")) Grid 
basicLayout = layoutHook defaultConfig

tallLayout = named "tall" $ avoidStruts $ basicLayout
wideLayout = named "wide" $ avoidStruts $ Mirror basicLayout
singleLayout = named "single" $ avoidStruts $ noBorders Full
fullscreenLayout = named "fullscreen" $ noBorders Full

myLayoutHook = onWorkspaces ["8:chat"] imLayout $
               onWorkspaces ["6:media"] fullscreenLayout $
               tallLayout ||| wideLayout ||| singleLayout

myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
  ----------------------------------------------------------------------
  -- Custom key bindings
  --

  -- Start a terminal.  Terminal to start is specified by myTerminal variable.
  [ ((modMask .|. shiftMask, xK_Return),
     spawnHere $ XMonad.terminal conf)

  -- run google chrome
  , ((modMask, xK_w),
     spawnHere $ getProgram "browser")
  -- @@ Move pointer to currently focused window
  , ((modMask,   xK_z     ), warpToWindow (1%2) (1%2)) 

  , ((modMask, xK_i), namedScratchpadAction scratchpads "spotify")

  -- Lock the screen using xscreensaver.
  , ((modMask .|. controlMask, xK_l),
     spawnHere $ getProgram "lock")

  -- Use this to launch programs without a key binding.
  , ((modMask, xK_p),
     spawnHere "dmenu_run -l 8 -nb \"#1B1D1E\" -nf \"#a0a0a0\" -sb \"#333\" -sf \"#fff\" -p Command: -b")

  -- Take a screenshot in select mode.
  -- After pressing this key binding, click a window, or draw a rectangle with
  -- the mouse.
  , ((modMask .|. shiftMask, xK_p),
     spawnHere "select-screenshot")

  -- Take full screenshot in multi-head mode.
  -- That is, take a screenshot of everything you see.
  , ((modMask .|. controlMask .|. shiftMask, xK_p),
     spawnHere "screenshot")

  -- Spotify Key bindings
  , ((mod4Mask, xK_c),
     spawnHere $ getProgram "media-pauseplay")

  , ((mod4Mask, xK_x),
     spawnHere $ getProgram "media-previous")

  , ((mod4Mask, xK_v),
     spawnHere $ getProgram "media-next")

  -- Mute volume.
  , ((0, 0x1008FF12),
     spawnHere "amixer -q set Front toggle")

  -- Decrease volume.
  , ((0, 0x1008FF11),
     spawnHere "amixer -q set Front 10%-")

  -- Increase volume.
  , ((0, 0x1008FF13),
     spawnHere "amixer -q set Front 10%+")

  -- Audio previous.
  , ((0, 0x1008FF16),
     spawnHere "")

  -- Play/pause.
  , ((0, 0x1008FF14),
     spawnHere "")

  -- Audio next.
  , ((0, 0x1008FF17),
     spawnHere "")

  -- Eject CD tray.
  , ((0, 0x1008FF2C),
     spawnHere "eject -T")

  --------------------------------------------------------------------
  -- "Standard" xmonad key bindings
  --

  -- Close focused window.
  , ((modMask .|. shiftMask, xK_c),
     kill)

  -- Cycle through the available layout algorithms.
  , ((modMask, xK_space),
     sendMessage NextLayout)

  --  Reset the layouts on the current workspace to default.
  , ((modMask .|. shiftMask, xK_space),
     setLayout $ XMonad.layoutHook conf)

  -- Resize viewed windows to the correct size.
  , ((modMask, xK_n),
     refresh)

  -- Move focus to the next window.
  , ((modMask, xK_Tab),
     windows W.focusDown)

  -- Move focus to the next window.
  , ((modMask, xK_j),
     windows W.focusDown)

  -- Move focus to the previous window.
  , ((modMask, xK_k),
     windows W.focusUp  )

  -- Move focus to the master window.
  , ((modMask, xK_m),
     windows W.focusMaster  )

  -- Swap the focused window and the master window.
  , ((modMask, xK_Return),
     windows W.swapMaster)

  -- Swap the focused window with the next window.
  , ((modMask .|. shiftMask, xK_j),
     windows W.swapDown  )

  -- Swap the focused window with the previous window.
  , ((modMask .|. shiftMask, xK_k),
     windows W.swapUp    )

  -- Shrink the master area.
  , ((modMask, xK_h),
     sendMessage Shrink)

  -- Expand the master area.
  , ((modMask, xK_l),
     sendMessage Expand)

  -- Push window back into tiling.
  , ((modMask, xK_t),
     withFocused $ windows . W.sink)

  -- Increment the number of windows in the master area.
  , ((modMask, xK_comma),
     sendMessage (IncMasterN 1))

  -- Decrement the number of windows in the master area.
  , ((modMask, xK_period),
     sendMessage (IncMasterN (-1)))

  ]
  ++
 
  -- mod-[1..9], Switch to workspace N
  -- mod-shift-[1..9], Move client to workspace N
  [((m .|. modMask, k), windows $ f i)
      | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
      , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
  ++

  -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
  -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
  [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
      | (key, sc) <- zip (SM.getScreenKeys screenSettings) myScreens
      , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

main = do
    xmproc <- spawnPipe $ "xmobar_restart -x " ++ (show $ toInteger $ SM.lookupScreenId screenSettings xmobarScreen)
    xmonad $ defaultConfig
        {    
             manageHook = myManageHooks <+> manageSpawn <+> manageDocks <+> namedScratchpadManageHook scratchpads <+> manageHook defaultConfig
           , startupHook = myStartupHook
           , layoutHook = myLayoutHook
		   , keys = myKeys
           , logHook = dynamicLogWithPP . namedScratchpadFilterOutWorkspacePP $ xmobarPP
                     { 
                      ppOutput          = hPutStrLn xmproc
                    , ppTitle           = xmobarColor "green" "" . shorten 50
                    , ppCurrent         = xmobarColor "yellow" "" . wrap "[" "]"
                    , ppVisible         = xmobarColor "white" "" . wrap "<" ">"
                    , ppWsSep           = " | "
                    , ppHiddenNoWindows = xmobarColor "grey" ""
                    , ppHidden          = xmobarColor "white" ""
                    , ppSort            = mkWsSort myWorkspaceCompare
                    }
           , borderWidth    = myBorderWidth
           , terminal       = getProgram "terminal"
           , workspaces     = myWorkspaces
        }

-- Custom sort for Xmobar that will match the physical layout of your montiors as defined by myScreens
myScreens = SM.getScreenIds screenSettings

myScreenSortVals = [ snd x | x <- (sort $ zip myScreens [1..])]
myWorkspaceCompare :: X WorkspaceCompare
myWorkspaceCompare = do
    w <- gets windowset
    return $ \ a b -> case (isOnScreen a w, isOnScreen b w) of
        (True, True)   -> compare (myScreenSortVals !! (fromIntegral ((tagToSid (onScreen w)) a))) (myScreenSortVals !! (fromIntegral ((tagToSid (onScreen w)) b)))
        (False, False) -> compare a b 
        (True, False)  -> LT
        (False, True)  -> GT
  where
    onScreen w =  W.current w : W.visible w
    isOnScreen a w  = a `elem` map (W.tag . W.workspace) (onScreen w)
    tagToSid s x = W.screen $ fromJust $ find ((== x) . W.tag . W.workspace) s
