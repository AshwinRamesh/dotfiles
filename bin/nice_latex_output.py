#!/usr/bin/env python

import sys
import re

mainfile = ""


def skipto_data(find,start):
	i = start + 1
	p = re.compile(find)

	while p.search(data[i]) is None:
		i += 1
	return i
	
data = sys.stdin.read().split("\n")

output = [data[0]]

upto = skipto_data( "LaTeX2e <" , 0 )
output[0] +=  " " + data[upto]

output += [""]
mainfile = data[upto - 1][1:]
upto = skipto_data( "Document Class: " , upto )
output += [ data[upto]]

for line in data[upto:]:
	if ".tex" in line:
		output += [ "\tbuilding: " + x for x in re.findall(r"\./[^\.]*\.tex",line)]
#		output += [ "\tbuilding: " + x.strip().replace("(","").replace(")","") for x in re.findall(r"(\([^)]*\.tex\))",line)]

tmp = []
flg = False
for line in data[upto:]:
	if ".tex" in line:
		if re.search(".tex:[0-9]*:",line) is not None:
			if "Fatal error" not in line:
				if ( mainfile not in line):
					flg = True
				tmp += [ "\t\t" + line ]

if ( not flg):
	output += ["\tbuilding: " + mainfile]

output += tmp + [""]

if "==>" not in data[-3] and ".pdf" not in data[-3]:
	data = data[:-4] + [data[-4] + data[-3]] + data[-2:]

if "==>" in data[-3]:
   	data[-3] = data[-3].split("==>")[1].strip()
output += ["\t" + x[:-1] for x in data[-3:-1][::-1]]

print "\n".join(output)
