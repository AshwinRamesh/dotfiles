import os, sys

base_dir = os.path.abspath(os.path.dirname(__file__))

def maybe_prune(filename):
    if os.path.islink(filename):
        target = os.readlink(filename)

        if target.startswith(base_dir) and not os.path.exists(target):
            print "removing", filename
            os.remove(filename)

def main():
    for dirname, dirnames, filenames in os.walk(os.path.expanduser("~")):
        for subdirname in dirnames:
            maybe_prune(dirname + "/" + subdirname)

        for filename in filenames:
            maybe_prune(dirname + "/" + filename)

if __name__ == "__main__":
    main()
