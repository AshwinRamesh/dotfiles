# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="risto"
# possible:
# - jreese = no hash :( + bad return code
# - gentoo  = No hash :(
# - mortalscumbag
# - muse

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

DISABLE_CORRECTION="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git git-flow archlinux python)

source $ZSH/oh-my-zsh.sh

#PROMPT='%{$fg[green]%}%n@%m:%{$fg_bold[blue]%}%~ $(git_prompt_info)%{$reset_color%}%(!.#.$) '

local return_code="%(?..%{$fg[red]%}%?%{$reset_color%})"
RPS1="${return_code}"

function git_prompt_info() {
    ref=$(git rev-parse --abbrev-ref HEAD 2> /dev/null) || return

    BRANCH=$(git rev-parse --abbrev-ref HEAD 2> /dev/null | grep -v 'HEAD')
    if [[ -z "$BRANCH" ]]; then
        BRANCH="%{$fg[red]%}Detached"
    else
        BRANCH="%{$fg[green]%}$(current_branch)"
    fi

    STASH=$(git stash list 2> /dev/null | wc -l | grep -v '^0$')
    if [[ -z "$STASH" ]]; then
        STASH=""
    else
        STASH=":%{$fg[blue]%}$STASH%{$reset_color%}"
    fi

    echo "<%{$fg[red]%}$(git_prompt_short_sha)%{$reset_color%}:$BRANCH%{$reset_color%}$STASH>"
}

# Customize to your needs...
export PATH=$PATH:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:$HOME/.xmonad/bin:$HOME/.bin
export TERM='rxvt-256color'
export EDITOR='vim'

# enable extended globbing
setopt extended_glob

bindkey "^[[7~" beginning-of-line
bindkey "^[[8~" end-of-line

# Aliases
alias -r fmain='grep "int main" *'

alias -r gccw='gcc -ansi -pedantic -Wall -w '
alias -r g++w='g++ -pedantic -Wall -w '

alias -r ll='ls -l'
alias -r la='ls -la'
alias -r c='clear'
alias -r cls="clear && ls"
alias -r sc='screen -rd || screen'
alias -r scr='screen -rd rtorrent'
alias -r sss='ssh server'
alias -r up='cd ..'

alias -r g='/usr/bin/git status'
alias -r gu='/usr/bin/git pull'
alias -r gp='/usr/bin/git push'
alias -r ga='/usr/bin/git add'
alias -r gd='/usr/bin/git diff'
alias -r gb='/usr/bin/git branch'
alias -r gba='/usr/bin/git branch -a'
alias -r gc='/usr/bin/git commit'
alias -r gca='/usr/bin/git commit -a'
alias -r gco='/usr/bin/git checkout'
alias -r gl="/usr/bin/git log --date-order --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative"

alias -r gm="/usr/bin/git merge --no-ff"
alias -r gff="/usr/bin/git merge --ff-only"

alias -r gdb='gdb -q -ex run'
alias -r vim='vim -O'

alias -r mysql='mysql --sigint-ignore'
alias -r mysqll='mysql --sigint-ignore -u root -p'
alias -r psqll='sudo -u postgres psql'

alias -r ipmasq='sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE'

alias -r vagrant='/opt/vagrant/bin/vagrant'

if [[ -f '/usr/bin/vendor_perl/ack' ]]; then
    alias ack-grep='/usr/bin/vendor_perl/ack'
fi

alias -r ls="ls --ignore='*.pyc' --color"

eval $(ssh-agent) > /dev/null
ssh-add ~/.ssh/id_rsa 2> /dev/null
