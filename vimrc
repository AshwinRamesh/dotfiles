" This must be first, because it changes other options as side effect
set nocompatible

syntax enable

let g:snips_author='Nick Davies'

set bg=dark
set ruler
set display+=lastline
set nowrap
set linebreak
set hlsearch
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set ic
set wrapscan
set virtualedit=all
set showmatch
set number
set smartindent
set autoindent
set nofoldenable 

set noswapfile


au VimResized * exe "normal! \<c-w>="

:filetype plugin on
:filetype indent on
let g:tex_flavor='latex'

command! WQA :wqa
command! WqA :wqa
command! WQa :wqa
command! Wqa :wqa
command! WA :wa
command! Wa :wa
command! WQ :wq
command! Wq :wq
command! W :w
command! Wn :wn
command! WN :wn
command! Wp :wp
command! WP :wp
command! QA :qa
command! Qa :qa
command! Q :q

nmap ; :

set listchars=tab:>.,trail:~

nmap <silent> <c-n> :NERDTreeToggle<CR>
map <F7> :w<CR>:!ispell -x -d british %<CR><CR>:e<CR><CR>  

let NERDTreeShowBookmarks=1

let @i=":s/^\\([\\t ]*\\)/\\1\\\\item /\r:\'<,\'<s/^[ \\t]*/&\\\\begin{itemize}\\r&/\r:\'>,\'>s/^\\([ \\t]*\\).*/&\\r\\1\\\\end{itemize}/\r:'<,'>s/\\\\item\\([ \\t]*\\\\item\\)*/\\t\\\\item/\r:nohl\r"
let @o=":s/\\\\item [–•] /\\\\item /\r:nohl\r"

let @c="   /**FFF\n    * \n    *\n    * \n    *\n    * @name	\n    * @access	public\n    * @param	\n    * @return	\n    */\n\n\n    \/\/ --------------------------------------------------------------------"

" Fuck you, help key.
noremap  <F1> :set invfullscreen<CR>
inoremap <F1> <ESC>:set invfullscreen<CR>a

 " Fuck you too, manual key.
nnoremap K <nop>

" Stop it, hash key.
inoremap # X<BS>#

vnoremap < <gv
vnoremap > >gv

nnoremap j gj
nnoremap k gk

autocmd FileType gitcommit DiffGitCached | wincmd L | wincmd p

inoremap jk <Esc>

call pathogen#infect()
call pathogen#helptags()
